function stability = plot_euler_solutions(methodTitle)

    % Create time steps, end time and initial condition.

    y_0=1;
    t_end=5;

    dt1=0.5;
    dt2=dt1/2;
    dt3=dt2/2;
    dt4=dt3/2;
    dt5=dt4/2;

    t1=0:dt1:t_end;
    t2=0:dt2:t_end;
    t3=0:dt3:t_end;
    t4=0:dt4:t_end;
    t5=0:dt5:t_end;

    % Create solutions at different timesteps calling the
    % appropriate scheme.

    switch methodTitle
        case 'Explicit Euler'
            y1=expl_euler(y_0,dt1,t_end,@dahlquist);
            y2=expl_euler(y_0,dt2,t_end,@dahlquist);
            y3=expl_euler(y_0,dt3,t_end,@dahlquist);
            y4=expl_euler(y_0,dt4,t_end,@dahlquist);
            y5=expl_euler(y_0,dt5,t_end,@dahlquist);
        case 'Implicit Euler'
            y1=impl_euler(y_0,dt1,t_end,@dahlquist,@d_dahlquist);
            y2=impl_euler(y_0,dt2,t_end,@dahlquist,@d_dahlquist);
            y3=impl_euler(y_0,dt3,t_end,@dahlquist,@d_dahlquist);
            y4=impl_euler(y_0,dt4,t_end,@dahlquist,@d_dahlquist);
            y5=impl_euler(y_0,dt5,t_end,@dahlquist,@d_dahlquist);
    end

    
    % Calculate the exact solution.

    y=@(t) exp(-7*t);
    y_exact=y(t5);

    % Calculate the approximation error.

    e(1)=error_calculation(t1,y1,y);
    e(2)=error_calculation(t2,y2,y);
    e(3)=error_calculation(t3,y3,y);
    e(4)=error_calculation(t4,y4,y);
    e(5)=error_calculation(t5,y5,y);

    % Calculate the error reduction factor.

    factors=ones(1,5);
    factors(2:5)=e(1:4)./e(2:5);

    % Stability
    stability = ones(5,1);
    if sum(y1<0)/length(y1) > 1/3
        stability(1) = ' ';
    else
        stability(1) = 'X';
    end
    if sum(y2<0)/length(y2) > 1/3
        stability(2) = ' ';
    else
        stability(2) = 'X';
    end
    if sum(y3<0)/length(y3) > 1/3
        stability(3) = ' ';
    else
        stability(3) = 'X';
    end
    if sum(y4<0)/length(y4) > 1/3
        stability(4) = ' ';
    else
        stability(4) = 'X';
    end
    if sum(y5<0)/length(y5) > 1/3
        stability(5) = ' ';
    else
        stability(5) = 'X';
    end

    % Plot the obtained solutions.

    figure
    plot(t1,y1,t2,y2,t3,y3,t4,y4,t5,y5,t5,y_exact);
    title(methodTitle);
    axis([0 5 -1 1])
    legend();
    legend("dt=1/2","dt=1/4", "dt=1/8","dt=1/16","dt=1/32","Exact Solution");

    % Display error and error reduction as a table.
    
    time_step=["dt=1/2","dt=1/4","dt=1/8","dt=1/16","dt=1/32"];
    T=table(e',factors',char(stability),'VariableNames',{'Error','Error Reduction','Stability'},'RowNames',time_step);
    T=table(T,'VariableNames',{methodTitle});
    disp(T);
    

end