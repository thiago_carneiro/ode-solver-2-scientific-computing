% Function that applies the explicit euler scheme to solve an ODE or System
% and returns the solution vector.
%
%   Inputs:
%            y_0 = initial condition; Can be scalar or vector.
%            dt = time step size; 
%            t_end = end time;
%            f = right hand side: y' = f(x) f is a function handle: f = @MyFunction;
%   Outputs:
%           y = solution vector/matrix. In the case of vectors


function y=expl_euler(y_0, dt, t_end, f)
    t=0:dt:t_end;
    y=zeros(length(y_0),1 + t_end/dt);
    y(:,1)=y_0;
    for i=2:1:length(t)
        y(:,i)=y(:,i-1)+dt*f(t(i),y(:,i-1));        
    end 
end