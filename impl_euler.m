% Function that applies the implicit euler scheme to solve an ODE or System
% and returns solution vector.
%
%   Inputs:
%            y_0 = initial condition; Can be scalar or vector.
%            dt = time step size; 
%            t_end = end time;
%            f = right hand side: y' = f(x) f is a function handle: f = @MyFunction;
%   Outputs:
%           y = solution vector/matrix.

function y=impl_euler(y_0, dt, t_end, f, df)
    t=0:dt:t_end;
    y=zeros(length(y_0),1+t_end/dt);
    y(:,1)=y_0;
    I=eye(length(y_0));

    for i=2:1:length(t)
        dG=@(x) I - dt * df(t(i),x);        
        G=@(x) x-dt*f(t(i),x)-y(:,i-1);
        y(:,i)=newton(y(:,i-1),G,dG);
        if y(:,i) == ones(length(y_0))*-inf
            fprintf('Implicit Euler method cannot find a solution for this problem with dt=%d\n',dt);
            y = ones(size(y)) * -inf;
            return
        end
    end
end