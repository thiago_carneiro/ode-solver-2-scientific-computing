function vdp_plot(methodTitle)

    % Initial conditions, time step and end time.

    y_0=[1;1];
    t_end=20;
    dt=0.1;
    t=0:dt:t_end;

    % Select which scheme to use and call the appropriate function.
    
    switch methodTitle
        case 'Explicit Euler'
            y=expl_euler(y_0,dt,t_end,@vanderpol_osc);
        case 'Implicit Euler'
            y=impl_euler(y_0,dt,t_end,@vanderpol_osc,@d_vdp);
    end
    
    % Plot the calculated solutions in  a single figure using
    % multiple subplots.

    figure
    
    tiledlayout(3,1)
    nexttile
    plot(t,y(1,:));
    title("Van-der-Pol Oscillator, x vs t");
    nexttile
    plot(t,y(2,:));
    title("Van-der-Pol Oscillator, y vs t");
    nexttile
    plot(y(1,:),y(2,:));
    title("Van-der-Pol Oscillator, x vs y");
    sgtitle(methodTitle)

end