function f = vanderpol_osc(t,y)
    f(1,1) = y(2,1);
    f(2,1) = 4*(1 - y(1,1)^2) * y(2,1) - y(1,1);
end