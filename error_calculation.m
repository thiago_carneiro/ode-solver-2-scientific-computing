% Function that computes the error of an approximated solution
% compared to the exact function.
%   Inputs:
%        t= time domain.
%        y= approximated solution
%   Output:
%        e=total approximated error

function e=error_calculation(t,y,exact_function)

        dt=t(2)-t(1);
        difs=y-exact_function(t);
        e=sqrt(dt*sum(difs.^2)/t(end));
end