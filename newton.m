% Function that applies the newton method to find the roots of a scalar or
% vector valued function.
%
%   Inputs:
%            x0 = initial condition;
%            G = scalar or vector valued function. G is a function handle: G = @MyFunction;
%            dG = derivative or jacobian of function. dG is a function handle: df = @MyFunction;
%   Outputs:
%            x=Approximate root.

function y=newton(x0,G,dG)
    warning('error', 'MATLAB:nearlySingularMatrix'); % make it into an error to be caught
    d=1;
    tol=10^-8;
    iters=0;
    while(d>tol && iters<100)
        try
        if dG(x0) == 0
            throw(MException('MATLAB:nearlySingularMatrix', 'dG = 0'));
        end
        y=x0-dG(x0)\G(x0);
        catch err
            switch err.identifier
                case 'MATLAB:nearlySingularMatrix'
                    fprintf(err.message);
                    y = ones(length(x0),1) * -inf;
                    return
            end
        end
        d=max(abs((y-x0)./y));
        iters=iters+1;
        x0=y;
    end
end