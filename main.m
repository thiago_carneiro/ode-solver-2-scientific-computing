function worksheet3()

clear;clc;

%    (a) Plot the explicit solutions in a single graph.
%    (e) Print the error and error reduction to the console in a
%        readable way.

    expl_stability = plot_euler_solutions('Explicit Euler');

%    (b) Implement the Newton method
%    (c) Implement the implicit euler method.
%    (d) Compute and plot implicit solutions in a single graph.
%    (e) Print the error and error reduction to the console in a
%        readable way.

    impl_stability = plot_euler_solutions('Implicit Euler');

    time_step=["dt=1/2","dt=1/4","dt=1/8","dt=1/16","dt=1/32"];
    T=table(char(expl_stability),char(impl_stability),'VariableNames',{'Explicit Euler','Implicit Euler'},'RowNames',time_step);
    T=table(T,'VariableNames',{'Stability'});
    disp(T);


%   (g) Try to solve vdp oscillator using the explicit scheme and plot.
%   The problem is not solveable using this timestep, the solution is
%   unphysical.

    vdp_plot('Explicit Euler');

%   (h) Extend the Newton solver to vectors.
%   (i) Solve the vdp oscillator using the implicit euler scheme and plot.
    
    vdp_plot('Implicit Euler');

    
end